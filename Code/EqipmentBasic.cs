using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal.Profiling.Memory.Experimental;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEditor.Experimental.AssetDatabaseExperimental.AssetDatabaseCounters;

public class EqipmentBasic : MonoBehaviour
{
    public string type = "Conveyer"; //��� ������������

    public EqipmentBasic nextEqipment; //��������� ������� ������������������
    public Vector3 equipmentPosition;
    public ItemInfo itemInfo; //������� � ������������ 
    public bool isLoaded; //���������
    public bool isDemo;

    private Vector3 dragOffset;
    private Camera  cam;

    private MovementController movementController;

    private void OnMouseDown()
    {
        GetInitialInfo(this);

        EqipmentBasic newEquipment = Instantiate<EqipmentBasic>(this, equipmentPosition, Quaternion.identity);
        //newEquipment.name = type;
        dragOffset = transform.position - GetMousePos();
        //gameObject.name = EquipmentEvent.EventGetName(this.type, this);
    }

    private void OnMouseDrag()
    {
        transform.position = GetMousePos() + dragOffset;
        if (Input.GetKeyUp(KeyCode.Q))
        {
            this.transform.Rotate(0f, 0f, 90);
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            this.transform.Rotate(0f, 0f, -90);
        }
    }

    private void OnMouseUp()
    {
        Vector3 pos = transform.position;
        pos.x = (float)Math.Round(pos.x);
        pos.y = (float)Math.Round(pos.y);
        if (pos.x < 0 || pos.y < -1 || pos.x > 19 || pos.y > 9)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.position = pos;
            gameObject.name = type;
            gameObject.name = EquipmentEvent.EventGetName(this.type, this);
            GetInitialInfo(this);
        }
    }
    Vector3 GetMousePos() 
    {
        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
    }

    public Vector3 GetItemPosition(GameObject gameObject) //��������������� ��������
    {
        var position = gameObject.transform.position;
        return new Vector3(position.x, position.y, position.z);
    }

    public bool GetIsLoaded(GameObject gameObject) //��������� ������������� ������������
    {
        return gameObject.GetComponent<EqipmentBasic>().isLoaded;
    }

    public EqipmentBasic GetNextEquipment(GameObject gameObject) //��������� �������� ���������� ������������
    {
        return gameObject.GetComponent<EqipmentBasic>().nextEqipment;
    }

    private IEnumerator StartBeltMove(ItemInfo equipmentItem, EqipmentBasic nextEqipment, Vector3 equipmentPosition, Vector3 nextEquipmentPosition, GameObject equipment) //�����������
    {
        nextEquipmentPosition.z = -2;
        if (equipmentItem.item != null && nextEqipment != null && nextEqipment.isLoaded == false)
        {
            var step = movementController.speed * Time.deltaTime;           
            while (equipmentItem.item.transform.position != nextEquipmentPosition)
            {
                equipmentItem.item.transform.position = Vector3.MoveTowards(equipmentItem.transform.position, nextEquipmentPosition, step);
                yield return null;
            }
        }
        equipment.gameObject.GetComponent<EqipmentBasic>().isLoaded = false;
        equipment.gameObject.GetComponent<EqipmentBasic>().nextEqipment.itemInfo = equipment.gameObject.GetComponent<EqipmentBasic>().itemInfo;
        equipment.gameObject.GetComponent<EqipmentBasic>().itemInfo = null;
        equipment.gameObject.GetComponent<EqipmentBasic>().nextEqipment.isLoaded = true;
    }

    private EqipmentBasic FindNextBelt(GameObject gameObject) //����������� ���������� ������������
    {
        LayerMask mask = LayerMask.GetMask("Equipment");
        Transform currentBeltTransform = gameObject.transform;
        RaycastHit hit;

        var forward = gameObject.transform.up;

        Ray ray = new Ray(currentBeltTransform.position, forward);

        if (Physics.Raycast(ray, out hit, 1f, mask))
        {
            EqipmentBasic belt = hit.collider.GetComponent<EqipmentBasic>();

            if (belt != null)
                return belt;
        }

        return null;
    }

    private void MoveToNext(ItemInfo equipmentItem, EqipmentBasic nextEqipment, Vector3 equipmentPosition, Vector3 nextEquipmentPosition, GameObject equipment) //������ ����������� ��������
    {
        if (equipmentItem!= null && equipmentItem.item != null)
        {
            StartCoroutine(StartBeltMove(equipmentItem, nextEqipment, equipmentPosition, nextEquipmentPosition, equipment));
        }
    }

    private void GetInitialInfo(EqipmentBasic eqipment)
    {
        GameObject gameObject = eqipment.gameObject;
        eqipment.equipmentPosition = GetItemPosition(gameObject);
        eqipment.isLoaded = GetIsLoaded(gameObject);
        eqipment.nextEqipment = FindNextBelt(gameObject);

        isDemo = false;
    }

    private void Start()
    {
        EquipmentEvent.EventMove = new EquipmentEvent.ItemMove(MoveToNext);
        EquipmentEvent.EventFindNextEquipment = new EquipmentEvent.FindNextEquipment(FindNextBelt);
        EquipmentEvent.GetInfo = new EquipmentEvent.GetInitialInfo(GetInitialInfo);

        movementController = FindObjectOfType<MovementController>();
        //movementController = FindObjectOfType<MovementController>();
        //equipmentPosition = GetItemPosition(this.gameObject);
        isDemo = false;
    }

    private void Update()
    {

    }

    private void Awake()
    {
        cam = Camera.main;
    }
}
