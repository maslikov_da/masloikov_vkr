using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputUnit : EqipmentBasic //�������� ����� ���������, ������������ �����
{
    ItemInfo itemToCreate; 
    public ItemInfo[] items; //��������� ��������� ��� �������� - �����������
    public int creatorID = 0;

    // Start is called before the first frame update
    void Start() //��������� ����������� ���������� � �����
    {
        EquipmentEvent.EventCreate = new EquipmentEvent.ItemCreate(CreateItem);

        type = "Input";
        equipmentPosition = GetItemPosition(gameObject);
        isLoaded = GetIsLoaded(gameObject);
        nextEqipment = GetNextEquipment(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateItem(int ID, Vector3 equipmentPosition, EqipmentBasic eqipment) // ������� �������
    {
        ItemInfo item = Instantiate<ItemInfo>(items[ID], equipmentPosition, Quaternion.identity);
        eqipment.gameObject.GetComponent<EqipmentBasic>().itemInfo = item;
        eqipment.gameObject.GetComponent<EqipmentBasic>().isLoaded = true;
    }
}
