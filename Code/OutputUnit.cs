using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class OutputEvent //������� ������������ ������
{
    public delegate int GetTarget(); //��������� ���������� � ����
    public static GetTarget EventTarget; 
}

public class OutputUnit : EqipmentBasic
{
    int DestroyItem(ItemInfo item, EqipmentBasic eqipment, Color target) //����������� ��������
    {
        if (target == item.GetComponent<SpriteRenderer>().color) //�������� �� ����
        {
            Destroy(item.gameObject);
            eqipment.isLoaded = false;
            return OutputEvent.EventTarget() + 1;
        }
        Destroy(item.gameObject);
        eqipment.isLoaded = false;
        return OutputEvent.EventTarget();
    }

    // Start is called before the first frame update
    void Start() //��������� ��������� ����������
    {
        EquipmentEvent.EventDestroy = new EquipmentEvent.ItemDestroy(DestroyItem);

        type = "Output";
        equipmentPosition = GetItemPosition(gameObject);
        nextEqipment = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
