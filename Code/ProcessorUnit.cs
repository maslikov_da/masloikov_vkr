using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessorUnit : EqipmentBasic //�������� ����� ���������, ������������ ����������
{
    public List<string> processorTypes; //�������� �����������
    public int processorTypeCode = 0; //ID �����������
    public string processorType; //��������� �����������
    // Start is called before the first frame update

    private void ItemChanger(GameObject item, ProcessorUnit processorUnit) //��������� - �����������
    {
        switch (processorUnit.processorType)
        {
            case ("RedToGreen"):
                item.GetComponent<SpriteRenderer>().color = Color.green;
                break;
            case ("GreenToBlue"):
                item.GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            case ("BlueToRed"):
                item.GetComponent <SpriteRenderer>().color = Color.red;
                break;
        }
    }

    void Start() // ��������� ����������� ����������
    {
        EquipmentEvent.EventChange = new EquipmentEvent.ItemChange(ItemChanger);
        EquipmentEvent.EventGetProcessorType = new EquipmentEvent.SetProcessorType(SetProcessorType);

        type = "Processor";
        processorTypes.Add("RedToGreen");
        processorTypes.Add("GreenToBlue");
        processorTypes.Add("BlueToRed");
        processorType = processorTypes[processorTypeCode];
        equipmentPosition = GetItemPosition(gameObject);
        isLoaded = GetIsLoaded(gameObject);
        nextEqipment = GetNextEquipment(gameObject);
    }

    private void SetProcessorType(ProcessorUnit processorUnit)
    {
        processorUnit.gameObject.GetComponent<ProcessorUnit>().processorType = processorUnit.gameObject.GetComponent<ProcessorUnit>().processorTypes[processorUnit.gameObject.GetComponent<ProcessorUnit>().processorTypeCode];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
