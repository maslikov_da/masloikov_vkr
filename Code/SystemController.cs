using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public static class EquipmentEvent //������� ������������!
{
    public delegate void ItemMove(ItemInfo equipmentItem, EqipmentBasic nextEqipment, Vector3 equipmentPosition, Vector3 nextEquipmentPosition, GameObject eqipment);
    public static ItemMove EventMove; //������� ��������

    public delegate EqipmentBasic FindNextEquipment(GameObject gameObject);
    public static FindNextEquipment EventFindNextEquipment; //������� ��������� �������� ���������� ������������

    public delegate void ItemChange(GameObject item, ProcessorUnit processorUnit);
    public static ItemChange EventChange; //������� ��������� ��������

    public delegate void ItemCreate(int ID, Vector3 equipmentPosition, EqipmentBasic eqipment);
    public static ItemCreate EventCreate; //������� �������� �������

    public delegate int ItemDestroy(ItemInfo item, EqipmentBasic eqipment, Color target);
    public static ItemDestroy EventDestroy; //������� ����������� ��������

    public delegate void GetInitialInfo(EqipmentBasic equipment);
    public static GetInitialInfo GetInfo;

    public delegate string GetName(string type, EqipmentBasic eqipment);
    public static GetName EventGetName;
    
    public delegate void SetProcessorType(ProcessorUnit processorUnit);
    public static SetProcessorType EventGetProcessorType;
}


public static class UIEvent //������� ����������
{
    public delegate UnityEngine.UI.Button GetButton();
    public static GetButton EventGetButton; //�������� ��������� ������
}

public class SystemController : MonoBehaviour
{
    public List<GameObject> InstalledEquipment; //��������� ������������
    public bool isFirstStep = true; //����������� ������� ����
    int counter = 0; //�������
    public int target, seconds, currentTarget; //���� ������ � ������� ��������
    public bool isTargetAchieved = false; //������������ ���������� ����
    public UnityEngine.UI.Button stepButton; //������
    Color targetItem = Color.green; //������� ������� - ��������� �������
    public bool isStarted, startButtonClicked, isStopped;

    private int conveyerID, outputID, processorID, inputID;

    void Start() //����������� ��� ������� 
    {
        target = 10; //��������� ���� - ��������� �������
        seconds = 1; //����� �������
        stepButton = UIEvent.EventGetButton(); //��������� �������� ������
        OutputEvent.EventTarget = new OutputEvent.GetTarget(Target);
        EquipmentEvent.EventGetName = new EquipmentEvent.GetName(GetName);

        isFirstStep = true;
    }

    string GetName(string type, EqipmentBasic eqipment)
    {
        string name = "";
        switch (type)
        {
            case "Input":
                name = eqipment.gameObject.name = $"{type + inputID++}";
                break;
            case "Processor":
                name = eqipment.gameObject.name = $"{type + processorID++}";
                break;
            case "Output":
                name = eqipment.gameObject.name = $"{type + outputID++}";
                break;
            case "Conveyer":
                name = eqipment.gameObject.name = $"{type + conveyerID++}";
                break;
        }
        return name;
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator waiter(int seconds) //����� � ���������� ������
    {
        yield return new WaitForSeconds(seconds);
        if (isTargetAchieved)
        {
            SimulationEnd();
            Debug.Log("Level Complited!");
            stepButton.interactable = false;
            isTargetAchieved = false;
        }
        else
            stepButton.interactable = true;
    }

    public int Target() //���������� ��������� ���������� � ����
    {
        return currentTarget;
    }

    public void GetList(string equipmentType) //��������� ���������� �� ������������� ������������
    {
        counter = 0;
        string equipmentName = equipmentType + counter;
        while (GameObject.Find(equipmentName) != null && isFirstStep)
        {
            InstalledEquipment.Add(GameObject.Find(equipmentName));
            InstalledEquipment[counter].GetComponent<EqipmentBasic>().nextEqipment = EquipmentEvent.EventFindNextEquipment(InstalledEquipment[counter]);
            counter++;
            equipmentName = equipmentType + counter;
        }
    }



    public void SimulationStep() //��� ���������
    {
        if (startButtonClicked == false)
            stepButton.interactable = false;
        counter = 0;

        if (isFirstStep)
        {
            GetList("Input");
            GetList("Conveyer");
            GetList("Processor");
            GetList("Output");
            foreach (GameObject equipment in InstalledEquipment) 
            {
                EqipmentBasic eqiupmentBasic = equipment.gameObject.GetComponent<EqipmentBasic>(); //������ �� ������������
                //equipment.gameObject.GetComponent<EqipmentBasic>().nextEqipment = EquipmentEvent.EventFindNextEquipment(equipment);
                EquipmentEvent.GetInfo(eqiupmentBasic);
            }
            isFirstStep = false;
        }

        foreach (GameObject equipment in InstalledEquipment)  //���������� ���������
        {
            EqipmentBasic eqiupmentBasic = equipment.gameObject.GetComponent<EqipmentBasic>(); //������ �� ������������
            EqipmentBasic nextEquipment = equipment.gameObject.GetComponent<EqipmentBasic>().nextEqipment; //������ �� ��������� ������������
            if (equipment.gameObject != null)
            {
                if (eqiupmentBasic.isDemo == false)
                {
                    if (eqiupmentBasic.type == "Input" && nextEquipment.isLoaded == false) //��������� ������������ �����
                    {
                        EquipmentEvent.EventCreate(0, eqiupmentBasic.equipmentPosition, eqiupmentBasic);
                    }

                    if (eqiupmentBasic.type == "Output" && eqiupmentBasic.isLoaded == true) //��������� ������������ ������
                    {
                        currentTarget = EquipmentEvent.EventDestroy(eqiupmentBasic.itemInfo, eqiupmentBasic, targetItem);
                        if (currentTarget == target)
                        {
                            isTargetAchieved = true;
                        }
                    }

                    if (eqiupmentBasic.isLoaded && nextEquipment != null && eqiupmentBasic.type != "Output") //��������� ��������� � ������������
                    {
                        if (nextEquipment.isLoaded == false)
                        {
                            if (eqiupmentBasic.type == "Processor")//����������
                            {
                                ProcessorUnit processorUnit = equipment.gameObject.GetComponent<ProcessorUnit>();
                                EquipmentEvent.EventGetProcessorType(processorUnit);
                                EquipmentEvent.EventChange(eqiupmentBasic.itemInfo.gameObject, processorUnit);
                            }
                            EquipmentEvent.EventMove(eqiupmentBasic.itemInfo, nextEquipment, eqiupmentBasic.equipmentPosition, nextEquipment.equipmentPosition, equipment); //�������� �������� ��� ����� ������������ �� ����������� ������
                        }
                    }
                }
            }
        }
        if (isStarted == false)
        {
            StartCoroutine(waiter(seconds)); //�����
        }
    }

    IEnumerator Simulation(int seconds) //����� � ���������� ������
    {
        SimulationStep();
        yield return new WaitForSeconds(seconds);
        if (isTargetAchieved)
        {
            SimulationEnd();
            Debug.Log("Level Complited!");
            stepButton.interactable = false;
            isTargetAchieved = false;
        }
        else
        {
            if (!isTargetAchieved == false)
                isStarted = true;
            SimulationStart();
        }
    }

    public void SimulationStart()
    {
        stepButton.interactable = false;
        isStarted = true;
        while (isStarted && isStopped == false) 
        {
            isStarted = false;
            StartCoroutine(Simulation(seconds));
        }
    }

    public void SimulationEnd() 
    {
        isStopped = true;
    }
}
