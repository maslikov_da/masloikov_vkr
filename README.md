# Masloikov_VKR



## Ссылки на диаграммы

Пояснительная диаграма:  https://drive.google.com/file/d/1Bsp9JWGppQdDWfvg7stMu2bQuaPCmTxS/view?usp=sharing

Концепт-диаграмма:       https://drive.google.com/file/d/1MRQl18trTBKEvBBOnu_QNR9sozZ4ETjV/view?usp=sharing

Use-case:                https://drive.google.com/file/d/1IwZdELYF514PvXOpcuE6IhI6LH1onM4w/view?usp=sharing

Диаграмма компонентов:   https://drive.google.com/file/d/1o0QoMADaVna9FAst6IgDuE0sTUW94_0B/view?usp=sharing

Диаграмма классов:       https://drive.google.com/file/d/1XFXr-RlKJSZWTy4JUPAosNLxWUog0zki/view?usp=sharing

BPMN:                    https://drive.google.com/file/d/1Ldu-6M6Qb8wDs_gmRMUUyWYdthAZgkXW/view?usp=sharing - Игра

https://drive.google.com/file/d/1WJxAFDKwRZ2n-x2ts3tlkHnImb08AytW/view?usp=sharing - Сборка клавиатуры

https://drive.google.com/file/d/1mQi81FAiJqv-7syqbyxax77jPuXD6HQ9/view?usp=sharing - Создание корпуса
                         
https://drive.google.com/file/d/1cPeVldXA2-KgNj-hvm_n-6J6dF183JMV/view?usp=sharing - Создание клавиш

https://drive.google.com/file/d/1yC2BheAkmT4tLApnN7yzxzQgHUZ8jnBs/view?usp=sharing - Создание печатной платы

## Ссылки на диаграммы (Доп)

    Игровая петля 
https://drive.google.com/file/d/1Wc1QTg0sIMpYk0EOUfz60-7DNY794Bye/view
